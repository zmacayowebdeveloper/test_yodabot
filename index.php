<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Test YodaBot</title>
  </head>
  <body>

  <div id="app">
    <div style="padding-right:70%">
      <ul v-for="msg in record" style="list-style:none; padding-left: 0;">
        <li v-if="msg.type == 1">{{ msg.text }}</li>
        <li v-else style="text-align: right; margin-right:10vh">{{ msg.text }}</li>
      </ul>
      <span>Writing...</span>

      <form>
          <input style="width:80%" type="text" v-model="newMessage" value="">
          <button style="width:10%" type="button" @click="addMessage">Send</button>

      </form>
    </div>
  </div>

    <?php

      if(isset($_POST['mensaje'])){
        $mensaje = $_POST['mensaje'];
      }
      $mensaje = '';

      var_dump($mensaje);

     ?>

  </body>
</html>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="module">

  import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js'

  axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
  axios.defaults.headers.common['Access-Control-Allow-Methods'] = '*';

  const app = new Vue({
    el: '#app',
    data: {
      record: [
        {
          type: 1,
          text: '',
        },

      ],
      newMessage: null
    },
    mounted() {

      if (localStorage.getItem('record') != null ) {
        try {
          this.record = JSON.parse(localStorage.getItem('record'));
        } catch(e) {
          localStorage.removeItem('record');
        }
      }
    },
    methods: {

      addMessage(){
        // asegurarse que se ha escrito algo
        if (!this.newMessage) {
          return;
        }


        this.send();
      },
      send(){

        //credenciales para autenticación
        const ibentaKey = 'nyUl7wzXoKtgoHnd2fB0uRrAv0dDyLC+b4Y6xngpJDY='
        const secret = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9qZWN0IjoieW9kYV9jaGF0Ym90X2VuIn0.anf_eerFhoNq6J8b36_qbD4VqngX79-yyBKWih_eA1-HyaMe2skiJXkRNpyWxpjmpySYWzPGncwvlwz5ZRE7eg'

        const url = 'https://api.inbenta.io/v1/auth'
        const body = {
          'secret': secret
        }

        axios.post(url, body, {
          headers: {
            'x-inbenta-key': ibentaKey,
            'Content-Type': 'application/json'
          },
        })
        .then((res) => {
          console.log(res.data)

          this.record.push({
             type: 2,
             text: this.newMessage
           });
          this.newMessage = '';
          const parsed = JSON.stringify(this.record);
          localStorage.setItem('record', parsed);
        })
        .catch((error) => {
          console.error(error)
        })

      }
    }
  })

</script>
